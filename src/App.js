import React from 'react';
import SearchEvents from "./components/SearchEvents";
import SearchArtist from "./components/SearchArtists";
import {BrowserRouter as Router} from 'react-router-dom';
import Route from 'react-router-dom/Route';
class App extends React.Component {
	render() {
		return (
			<Router>
      <Route path="/" exact strict render={
          () => {
            return ( <SearchArtist/>);
          }
        }/>
      <Route path="/events" exact strict render={
          () => {
            return ( <SearchEvents/>);
          }
        }/>
		</Router>
		);
	}
}

export default App;
