import React, { Component } from "react";

class Artist extends Component {


  render() {
    const { name, avatar, facebookId } = this.props;
    return (
      <center>
      <div className="card">
        {/* Card image */}
        <div className="view overlay">
          <img className="card-img-top" src={avatar} alt="Card image cap" />
          <a>
            <div className="mask rgba-white-slight" />
          </a>
        </div>
        {/* Card content */}
        <div className="card-body elegant-color white-text rounded-bottom">
          {/* Social shares button */}
          {/* Title */}
          <h4 className="card-title">{name}</h4>
          <hr className="hr-light" />
          {/* Text */}
          <p className="card-text white-text mb-4"><b>Facebook ID:</b> {facebookId}</p>
          {/* Link */}
          <a href="#" className="white-text d-flex justify-content-end"><h5>Find Events</h5></a>
        </div>
      </div>
      </center>
    );
  }
}


export default Artist;
