# ReactJS Live Search(RESPONSIVE) - Bands Search Api
# ScreenShots of Mobile & Desktop View Given Below

ReactJs project which do live search on the following end-points:

1. **/artists/{artistname}**  *Return Single Artist*

2. **/artists/{artistname}/events** *Return Events o Artist*

**BASE_URL**: rest.bandsintown.com

**API Documentation:** https://app.swaggerhub.com/apis/Bandsintown/PublicAPI/3.0.0#/artist%20information/artist


<br>

**Desktop View** 
-----------------

* Artist Search:

![image](https://i.ibb.co/CV9YQjm/Screenshot-5.png)


* Events Search

![!image](https://i.ibb.co/HNwVQWg/Screenshot-6.png)


<br>

**Mobile View** 
-----------------

* Artist Search:

![image](https://i.ibb.co/znqvYJs/Screenshot-7.png)


* Events Search

![!image](https://i.ibb.co/4VXJj4y/Screenshot-8.png)
